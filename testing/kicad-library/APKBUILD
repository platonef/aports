# Maintainer: Marian Buschsieweke <marian.buschsieweke@ovgu.de>
# Contributor: Marian Buschsieweke <marian.buschsieweke@ovgu.de>
pkgname=kicad-library
pkgver=6.0.9
pkgrel=0
pkgdesc="Kicad component and footprint libraries"
url="https://kicad.github.io/"
# limited by kicad
arch="noarch !riscv64 !s390x"
license="GPL-3.0-or-later"
makedepends="cmake samurai"
depends="kicad"
subpackages="$pkgname-3d:three_d"
source="
	https://gitlab.com/kicad/libraries/kicad-symbols/-/archive/$pkgver/kicad-symbols-$pkgver.tar.gz
	https://gitlab.com/kicad/libraries/kicad-footprints/-/archive/$pkgver/kicad-footprints-$pkgver.tar.gz
	https://gitlab.com/kicad/libraries/kicad-packages3D/-/archive/$pkgver/kicad-packages3D-$pkgver.tar.gz
	"
options="!check" # package only provides data files, so not tests possible

build() {
	cd "$srcdir"/kicad-symbols-$pkgver
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build

	cd "$srcdir"/kicad-footprints-$pkgver
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build

	cd "$srcdir"/kicad-packages3D-$pkgver
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install "$srcdir"/kicad-symbols-$pkgver/build
	DESTDIR="$pkgdir" cmake --install "$srcdir"/kicad-footprints-$pkgver/build
}

three_d() {
	DESTDIR="$subpkgdir" cmake --install "$srcdir"/kicad-packages3D-$pkgver/build

	# Remove .step version of 3D models; only .wrl versions are needed
	find "$subpkgdir" -name '*.step' -exec rm {} \;
}

sha512sums="
2cc44ead8029cd502cc5301b4515aa23275d32068e091a0a4a645f6fd9ed88f2b7d329d9faa7e66cde58cedeb47810c66db8e6f9313c2bbfe18e5a272f82a163  kicad-symbols-6.0.9.tar.gz
43436a61fb1be66dc6cd6079e49e7af0f7d374f074874fd008eb45c2b3faee596e102173a274064a536f11c4525f223c9e8c2a33969811414811a226e69affaa  kicad-footprints-6.0.9.tar.gz
fe014b35ce9071ed48d00481d509fba678dc0bea5170f626de9b18c0e0285cb441fcc635197da40af869e35c50104a9a69d991348d6031293ee166b734ba7cc9  kicad-packages3D-6.0.9.tar.gz
"
