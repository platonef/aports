# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=bolt
pkgver=0.9.4
pkgrel=0
pkgdesc="Thunderbolt 3 device manager"
url="https://gitlab.freedesktop.org/bolt/bolt"
arch="all"
license="LGPL-2.1-or-later"
makedepends="
	asciidoc
	bash
	eudev-dev
	glib-dev
	meson
	polkit-dev
	"
checkdepends="
	dbus
	py3-gobject3
	"
subpackages="$pkgname-doc"
source="https://gitlab.freedesktop.org/bolt/bolt/-/archive/$pkgver/bolt-$pkgver.tar.gz"

# tests hit a udf instruction, due to some test_notify_teardown pointer free
# being undefined. using a different allocator fixes it, so musl is being
# strict about the UB tests here, but the issue is not from the code itself
options="$options !check"

build() {
	abuild-meson \
		-Db_lto=true \
		-Dman=true \
		-Dsystemd=false \
		. output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
7a04a26c15bae373de82511ee87978524d6b208a7dca8af2bffabb9fca7982cc95de171e38c234dca603aa45a0099a555e6da4f2e550b1962ce8f632ee49e6d4  bolt-0.9.4.tar.gz
"
