# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=qt6-qtconnectivity
pkgver=6.4.1
pkgrel=0
pkgdesc="Provides access to Bluetooth hardware"
url="https://qt.io/"
arch="all"
license="LGPL-2.1-only AND LGPL-3.0-only AND GPL-3.0-only AND Qt-GPL-exception-1.0"
depends_dev="
	bluez-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	"
makedepends="$depends_dev
	cmake
	perl
	samurai
	"
subpackages="$pkgname-dev"
builddir="$srcdir/qtconnectivity-everywhere-src-${pkgver/_/-}"

case $pkgver in
	*_alpha*|*_beta*|*_rc*) _rel=development_releases;;
	*) _rel=official_releases;;
esac

source="https://download.qt.io/$_rel/qt/${pkgver%.*}/${pkgver/_/-}/submodules/qtconnectivity-everywhere-src-${pkgver/_/-}.tar.xz"

build() {
	export CFLAGS="$CFLAGS -g1 -flto=auto"
	export CXXFLAGS="$CXXFLAGS -g1 -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DINSTALL_BINDIR=lib/qt6/bin \
		-DINSTALL_DOCDIR=share/doc/qt6 \
		-DINSTALL_ARCHDATADIR=lib/qt6 \
		-DINSTALL_DATADIR=share/qt6 \
		-DINSTALL_INCLUDEDIR=include/qt6 \
		-DINSTALL_MKSPECSDIR=lib/qt6/mkspecs \
		-DINSTALL_EXAMPLESDIR=share/doc/qt6/examples
	cmake --build build --parallel
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
dd94337565407a5e09ff4845e688ad012867fc6410f18eb63ba503542c6267d62fb08c109e1e84febc5c7f86e389ebfc724c45ad99f3469617edd51b5caa8b7c  qtconnectivity-everywhere-src-6.4.1.tar.xz
"
