# Maintainer: psykose <alice@ayaya.dev>
pkgname=duckstation
pkgver=0_git20221110
pkgrel=0
_gitrev=dec28501ed5c85ccf7a1a602d47926ae4ff85741
pkgdesc="Fast PlayStation 1 emulator"
url="https://github.com/stenzek/duckstation"
# supported+passing arches
arch="aarch64 x86_64 x86"
# there's more, find them later
license="GPL-3.0-or-later AND GPL-2.0-or-later AND Apache-2.0"
makedepends="
	clang
	cmake
	curl-dev
	extra-cmake-modules
	fmt-dev
	libdrm-dev
	libxrandr-dev
	lld
	llvm
	mesa-dev
	minizip-dev
	qt6-qtbase-dev
	qt6-qttools-dev
	samurai
	sdl2-dev
	soundtouch-dev
	tinyxml2-dev
	vulkan-loader-dev
	wayland-dev
	xz-dev
	zlib-dev
	zstd-dev
	"
subpackages="$pkgname-nogui"
source="$pkgname-$_gitrev.tar.gz::https://github.com/stenzek/duckstation/archive/$_gitrev.tar.gz
	system-deps.patch
	"
builddir="$srcdir/duckstation-$_gitrev"

build() {
	export CC=clang
	export CXX=clang++
	export CFLAGS="$CFLAGS -flto=thin"
	export CXXFLAGS="$CXXFLAGS -flto=thin -I/usr/include/minizip -I/usr/include/soundtouch"
	export LDFLAGS="$LDFLAGS -fuse-ld=lld"

	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=Release \
		-DUSE_WAYLAND=ON \
		-DUSE_DRMKMS=ON \
		-DBUILD_NOGUI_FRONTEND=ON \
		-DENABLE_DISCORD_PRESENCE=OFF
	cmake --build build
}

check() {
	./build/bin/common-tests
}

package() {
	mkdir -p "$pkgdir"/usr/lib/duckstation/

	rm build/bin/common-tests
	cp -a build/bin/* "$pkgdir"/usr/lib/duckstation/

	install -Dm755 /dev/stdin "$pkgdir"/usr/bin/duckstation-qt <<-EOF
		#!/bin/sh
		/usr/lib/duckstation/duckstation-qt "\$@"
	EOF
	install -Dm755 /dev/stdin "$pkgdir"/usr/bin/duckstation-nogui <<-EOF
		#!/bin/sh
		/usr/lib/duckstation/duckstation-nogui "\$@"
	EOF
}

nogui() {
	pkgdesc="$pkgdesc (nogui version)"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/bin/duckstation-nogui
	amove usr/lib/duckstation/duckstation-nogui
}

sha512sums="
31e3f6c7e7740cdaa95359086d1208d9f97b9ca43469a38ed5a09a8666381d0cd6f04c0a7d2148c36e114a76e06cf54151c08537225ecd7d315bf3d077005823  duckstation-dec28501ed5c85ccf7a1a602d47926ae4ff85741.tar.gz
5b3183c79dcedc03ce696ab8b98d6fc96167350d5f9ea0af736d5b99af89072b0e8c23edd6429e772d60c38cfdddd938139e2b92dd060e0c8350808c0943216d  system-deps.patch
"
