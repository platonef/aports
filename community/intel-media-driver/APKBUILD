# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=intel-media-driver
pkgver=22.6.3
pkgrel=0
pkgdesc="Intel Media Driver for VAAPI - Broadwell+ iGPUs"
options="!check" # tests can't run in check(), only on install
url="https://github.com/intel/media-driver"
arch="x86_64"
license="BSD-3-Clause AND MIT"
makedepends="
	cmake
	intel-gmmlib-dev
	libpciaccess-dev
	libva-dev
	samurai
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/intel/media-driver/archive/intel-media-$pkgver.tar.gz"
builddir="$srcdir/media-driver-intel-media-$pkgver"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DINSTALL_DRIVER_SYSCONF=OFF \
		-DMEDIA_BUILD_FATAL_WARNINGS=OFF \
		$CMAKE_CROSSOPTS
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1158a8a158c44f3911b16524b6f1ff2dbf119d8c3108600ac89e904a0832209c940e90549e04e305f56308a6ac17eb905eef2e74f14db23760c7f91e5ef5a35d  intel-media-driver-22.6.3.tar.gz
"
