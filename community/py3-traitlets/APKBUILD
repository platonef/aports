# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=py3-traitlets
pkgver=5.5.0
pkgrel=1
pkgdesc="lightweight Traits like module"
url="https://traitlets.readthedocs.io/"
arch="noarch"
license="BSD-3-Clause"
depends="python3"
makedepends="py3-gpep517 py3-hatchling"
options="!check" # No test suite present
source="traitlets-$pkgver.tar.gz::https://github.com/ipython/traitlets/archive/$pkgver.tar.gz"
builddir="$srcdir/${pkgname#py3-}-$pkgver"

replaces="py-traitlets" # Backwards compatibility
provides="py-traitlets=$pkgver-r$pkgrel" # Backwards compatibility

build()	{
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

package() {
	local whl=dist/traitlets-$pkgver-py3-none-any.whl
	python3 -m installer --dest="$pkgdir" "$whl"
}

sha512sums="
298afd310f2d1c25b86f09ca1177f03da4834f4d054bfabacb3c18535b94769810bfecf4cb48e43dea2fa39617d6a5c985ce9db78af1419e814a8504c05ef04c  traitlets-5.5.0.tar.gz
"
