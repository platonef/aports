# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=flatbuffers
pkgver=22.11.23
pkgrel=0
pkgdesc="Memory Efficient Serialization Library"
url="https://google.github.io/flatbuffers/"
# armhf: blocked by bus error in tests
# s390x: segfaults in tests
arch="all !armhf !s390x"
license="Apache-2.0"
depends_dev="flatc=$pkgver-r$pkgrel"
makedepends="cmake samurai"
subpackages="$pkgname-dev flatc"
source="flatbuffers-$pkgver.tar.gz::https://github.com/google/flatbuffers/archive/v$pkgver.tar.gz
	locale-headers.patch
	"

# Bus error in armv7 as well but it has downstream users
case "$CARCH" in
	armv7) options="!check"
esac

# secfixes:
#   0:
#     - CVE-2020-35864

build() {
	cmake -B . -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DFLATBUFFERS_BUILD_SHAREDLIB=ON \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=MinSizeRel
	cmake --build .
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install .
	install -Dm755 flatc -t "$pkgdir"/usr/bin/
	rm "$pkgdir"/usr/lib/*.a
}

flatc() {
	pkgdesc="$pkgdesc (compiler)"

	amove usr/bin
}

sha512sums="
9a8506688ea1fb55bf138e728aff0e64d79955dabc751c3c757b28ebbafd7f066722bf31f748c8e23e695850bec6c67b532a9673e018ec36868ac236c7f2e50a  flatbuffers-22.11.23.tar.gz
756f338938b9064366744ad4bb67488f968d8921f688607ca7796af93264d005638941abc6cf1d0275f07eaee920a13e1073541dbff28f0f7d030692a36574d0  locale-headers.patch
"
