# Contributor: Dmitry Zakharchenko <dmitz@disroot.org>
# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
pkgname=pipe-viewer
pkgver=0.4.3
pkgrel=0
pkgdesc="Lightweight YouTube client that does not require an YouTube API key"
url="https://github.com/trizen/pipe-viewer"
arch="noarch"
license="Artistic-2.0"
makedepends="perl-module-build"
depends="
	perl-data-dump
	perl-json
	perl-libwww
	perl-lwp-protocol-https
	perl-term-readline-gnu
	perl-unicode-linebreak
	"
checkdepends="perl-test-pod"
subpackages="$pkgname-doc $pkgname-gtk"
source="$pkgname-$pkgver.tar.gz::$url/archive/$pkgver.tar.gz"

build() {
	perl Build.PL --gtk3
}

check() {
	./Build test
}

package() {
	./Build install --destdir "$pkgdir" --installdirs vendor
}

gtk() {
	depends="$pkgname perl-gtk3 perl-file-sharedir"
	pkgdesc="$pkgdesc (GTK interface)"

	amove usr/bin/gtk-pipe-viewer
	amove "usr/share/perl5/vendor_perl/auto/share/dist/WWW-PipeViewer/gtk-*"
	amove usr/share/perl5/vendor_perl/auto/share/dist/WWW-PipeViewer/icons
}

sha512sums="
c4bd41b2d3ad8c56d95f4c38cdc4edd93b92342f4251035f7677f8968f14659f1234979a9fc40f00ba429b883a31d735b3c1befe183f4e1bbcd5c11cf1bcb13e  pipe-viewer-0.4.3.tar.gz
"
