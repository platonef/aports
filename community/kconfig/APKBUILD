# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kconfig
pkgver=5.100.1
pkgrel=0
pkgdesc="Configuration system"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later AND LGPL-2.0-only AND LGPL-2.1-or-later"
makedepends="
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qtdeclarative-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kconfig-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E 'kconfig(core-(kconfig|kdesktopfile)|gui-kstandardshortcutwatcher)test'
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1e704fe2e880d591326ed5e9d1af83bd3e576c62b799cd67f1af3524e3a3b3f8f0acacdfa20a76c0bfe2d47ee30138e6444cb4ff344d3b63be8151f319860513  kconfig-5.100.1.tar.xz
"
