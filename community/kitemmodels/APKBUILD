# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kitemmodels
pkgver=5.100.0
pkgrel=0
pkgdesc="Models for Qt Model/View system"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-only AND LGPL-2.0-or-later"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qtdeclarative-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kitemmodels-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	# kdescendantsproxymodel_smoketest and kdescendantsproxymodeltest are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kdescendantsproxymodel(_smoketest|test)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f147592c0a9223ea3e494ed373b753f0ae167a6e2f9c5458124771dadbfa7da663f22ac7a262da385d1c77b0e7e17412a18ef8ee7bcea1359ecc42b888385edc  kitemmodels-5.100.0.tar.xz
"
