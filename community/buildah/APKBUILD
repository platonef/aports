# Contributor: kohnish <kohnish@gmx.com>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=buildah
pkgver=1.28.1
pkgrel=0
pkgdesc="tool that facilitates building OCI container images"
url="https://github.com/containers/buildah"
license="Apache-2.0"
arch="all"
depends="crun shadow-subids fuse-overlayfs slirp4netns containers-common"
makedepends="go go-md2man lvm2-dev gpgme-dev libseccomp-dev btrfs-progs-dev bash"
subpackages="$pkgname-doc"
options="!check" # tests require root privileges
source="https://github.com/containers/buildah/archive/v$pkgver/buildah-$pkgver.tar.gz"

# secfixes:
#   1.28.0-r0:
#     - CVE-2022-2990
#   1.21.3-r0:
#     - CVE-2021-3602
#   1.19.4-r0:
#     - CVE-2021-20206
#   1.14.4-r0:
#     - CVE-2020-10696

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	GIT_COMMIT="$pkgver" make
}

package() {
	GIT_COMMIT="$pkgver" make install PREFIX=/usr DESTDIR="$pkgdir"
}

sha512sums="
c674a90a0cae2977b76c2b68c06a0006f89d74b5b41362c2512a3f8d7ba515c3bd693648a0ba955dad7bea1a43da6890d80855e1283f0fc0136d16d77a798fba  buildah-1.28.1.tar.gz
"
