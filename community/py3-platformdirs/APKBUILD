# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-platformdirs
pkgver=2.5.4
pkgrel=1
pkgdesc="Module for determining appropriate platform-specific dirs"
url="https://github.com/platformdirs/platformdirs"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-gpep517 py3-hatchling py3-hatch-vcs"
checkdepends="py3-appdirs py3-pytest py3-pytest-mock"
source="https://files.pythonhosted.org/packages/source/p/platformdirs/platformdirs-$pkgver.tar.gz"
builddir="$srcdir/platformdirs-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	python3 -m installer -d testenv \
		dist/platformdirs-$pkgver-py3-none-any.whl
	local sitedir="$(python3 -c 'import site;print(site.getsitepackages()[0])')"
	PYTHONPATH="$PWD/testenv/$sitedir" python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/platformdirs-$pkgver-py3-none-any.whl
}

sha512sums="
1b7ff79a421bc0b0320a9b1ba8f99dbbecd34a9bea7fa7696b58007f2e05d8a910a3f32e2c03e6e3a8f3d272ab1c045051a682e3155c5bc5285e66e4add59489  platformdirs-2.5.4.tar.gz
"
